<?php

require 'Frog.php';
require 'Ape.php';

$sheep = new Animal("shaun");
echo "Name: " . $sheep->name . "</br>"; // "shaun"
echo "Legs: " . $sheep->legs . "</br>"; // 4
echo "Cold Blooded: " . $sheep->cold_blooded . "</br>"; // "no"
echo "</br>";
$kodok = new Frog("buduk");
echo "Name: " . $kodok->name . "</br>";
echo "Legs: " . $kodok->legs . "</br>";
echo "Cold Blooded: " . $kodok->cold_blooded . "</br>";
echo "Jump: " . $kodok->jump() . "</br>";
echo "</br>";
$monkey = new Ape("kera sakti");
echo "Name: " . $monkey->name . "</br>";
echo "Legs: " . $monkey->legs . "</br>";
echo "Cold Blooded: " . $monkey->cold_blooded . "</br>";
echo "Yell: " . $monkey->yell() . "</br>";