<?php

require 'Animal.php';

class Frog extends Animal 
{
    public function jump()
    {
        return 'hop hop';
    }
}